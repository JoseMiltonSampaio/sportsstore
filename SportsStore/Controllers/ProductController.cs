using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models.Interfaces;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        public int PageSize = 4;

        public ProductController(IProductRepository repo)
        {
            repository = repo;
        }

        public ActionResult List(int page = 1)
        {
            var products = repository.Products
                .OrderBy(p=>p.ProductID)
                .Skip((page-1) * PageSize)
                .Take(PageSize);

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                ItemsPerPage = PageSize,
                TotalItems = repository.Products.Count()
            };

            var productsListViewModel = new ProductsListViewModel
            {
                PagingInfo = pagingInfo,
                Products = products
            };

            return View(productsListViewModel);
        }
        
    }
}